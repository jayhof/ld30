﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour
{
	private float lifetime = 2.0f;


	void Awake ()
	{
		Destroy(gameObject, lifetime);
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (!GameState.Instance.playing) {
			return;
		}
	}
	
	void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.gameObject.tag == "Player") {
			return;
		}

		if (coll.gameObject.tag == "Enemy") {
			coll.gameObject.GetComponent<Enemy>().ApplyDamage(1);
		}

		Destroy(gameObject);
	}


	public void Flip()
	{
		transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
	}
}
