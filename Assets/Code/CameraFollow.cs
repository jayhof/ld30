﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
//	public Transform[] transforms;
	
	public Transform target;


	void Start()
	{
//		transforms[0] = transform;
	}

	// Update is called once per frame
	void Update ()
	{
		transform.position = new Vector3(target.position.x, target.position.y, transform.position.z);
//		for(int i = 0; i < transforms.Length; i++)
//		{
//			transforms[i].position = new Vector3(target.position.x, target.position.y, transforms[i].position.z);
//		}
	}
}
