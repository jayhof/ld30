﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Rigidbody2D))]

public class CharacterController2D : MonoBehaviour
{
	private Rigidbody2D _rigidbody;

	private Player _currentPlayer;
	private RedPlayer redPlayer;
	private GreenPlayer greenPlayer;

	private Ripple _currentRipple;

	public bool isGrounded { get; private set; }
	public bool isWalled { get; private set; }
	public bool isInRipple { get; private set; }

	public Vector2 velocity { get; private set; }

	public LayerMask groundLayers;
	public LayerMask rippleLayers;

	public GameController gameController;


	void Awake()
	{
		isGrounded = false;
		isWalled = false;

		isInRipple = false;
		_currentRipple = null;

		redPlayer = GetComponentInChildren<RedPlayer>();
		greenPlayer = GetComponentInChildren<GreenPlayer>();

		_currentPlayer = greenPlayer;
		redPlayer.gameObject.SetActive(false);
	}


	// Use this for initialization
	void Start ()
	{
		_rigidbody = GetComponent<Rigidbody2D>();
	}

	void FixedUpdate()
	{
		isGrounded = Physics2D.OverlapCircle(_currentPlayer.groundCheck.position, 0.2f, groundLayers);
		isWalled = Physics2D.OverlapCircle(_currentPlayer.wallCheck.position, 0.25f, groundLayers);
	}

	// Update is called once per frame
	void Update ()
	{
		if (!GameState.Instance.playing)
			return;

		_rigidbody.velocity = new Vector2(velocity.x, _rigidbody.velocity.y);
	}

	void OnDrawGizmos()
	{
		if(_currentPlayer != null) {
			Gizmos.color = Color.white;
			Gizmos.DrawWireSphere(_currentPlayer.groundCheck.position, 0.25f);
			Gizmos.DrawWireSphere(_currentPlayer.wallCheck.position, 0.25f);
//			Gizmos.DrawWireCube(_currentPlayer.collider2D.transform.position, _currentPlayer.collider2D.bounds.size);
		}
	}
	

	public void SetHorizontalForce(float x)
	{
		velocity = new Vector2(x, velocity.y);
	}

	public void SetVerticalForce(float y)
	{
		velocity = new Vector2(velocity.x, y);
	}

	public void Jump()
	{
		_rigidbody.AddForce(new Vector2(0, _currentPlayer.jumpForce));
	}

	public void Jet()
	{
		float topSpeed = 8f;
		GameState.Instance.fuel -= 3.0f * Time.deltaTime;

		if (_rigidbody.velocity.y <= topSpeed) {
			_rigidbody.AddForce(new Vector2(0, 40f));
		}
	}


	void OnTriggerEnter2D(Collider2D other)
	{
		Collectable collectable = other.GetComponent<Collectable>();

		if (collectable != null) {

			switch (collectable.type)
			{
				case Collectable.Type.Ammo:
				GameState.Instance.ammo += collectable.intAmount;
				break;

			    case Collectable.Type.HealthPack:
					GameState.Instance.health += collectable.floatAmount;
					if(GameState.Instance.health > 100f)
						GameState.Instance.health = 100f;
				break;

				case Collectable.Type.Fuel:
				GameState.Instance.fuel = Mathf.Clamp(GameState.Instance.fuel + collectable.floatAmount, 0.0f, 100.0f);
				break;
			}

			collectable.PickUp();
			GetComponent<AudioSource>().Play ();
		}
	}


	void OnTriggerStay2D(Collider2D other)
	{
		Ripple ripple = other.GetComponent<Ripple>();

		if (ripple != null) {
			_currentRipple = ripple;
			isInRipple = true;
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		Ripple ripple = other.GetComponent<Ripple>();

		if (ripple != null) {
			_currentRipple = null;
			isInRipple = false;
		}
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		if(collision.gameObject.layer == LayerMask.NameToLayer("Death"))
		{
			GameState.Instance.health = 0;
		}
	}


	public float Switch()
	{
		Vector2 newPosition = new Vector2(_currentRipple.exit.transform.position.x, _currentRipple.exit.transform.position.y + 5.0f);
		float distance = transform.position.y - newPosition.y;
		transform.position = newPosition;

		if (_currentPlayer.type == Player.Type.RedPlayer) {
			greenPlayer.gameObject.SetActive(true);
			redPlayer.gameObject.SetActive(false);
			_currentPlayer = greenPlayer;
		} else {
			redPlayer.gameObject.SetActive(true);
			greenPlayer.gameObject.SetActive(false);
			_currentPlayer = redPlayer;
		}

		return distance;
	}
}
