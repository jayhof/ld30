﻿using UnityEngine;
using System.Collections;

public class Collectable : MonoBehaviour
{
	public enum Type
	{
		HealthPack,
		Fuel,
		Ammo
	}

	public Type type;
	public GameObject floatingText;

	public float floatAmount = 1f;
	public int intAmount = 1;



	public void PickUp()
	{
		string text = "+ ";
		
		switch (type)
		{
		case Type.HealthPack:
			text += floatAmount + " Health restored";
			break;
			
		case Type.Ammo:
			text += intAmount + " Bullets";
			break;
			
		case Type.Fuel:
			text += floatAmount + " Fuel";
			break;
		}
		
		floatingText.GetComponent<FloatingText>().text = text;
		GameObject newFloatingText = Instantiate(floatingText, new Vector3(transform.position.x, transform.position.y + 0.8f, 0), Quaternion.identity) as GameObject;

		Destroy(gameObject);
	}
}
