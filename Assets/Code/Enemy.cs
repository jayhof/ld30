﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public GuidedMissile prefabShot;

	public CharacterController2D target;

	public int health = 2;

	public bool isBoss = false;

	private bool _isFacingRight = true;

	// Use this for initialization
	void Start () {
		InvokeRepeating("LaunchProjectile", 2, 2f);
	}

	void Update() {
		if(health <= 0) {
			Destroy(gameObject);
			if(isBoss)
				Application.LoadLevel("Credits");
		}

		if(target.transform.position.x <= transform.position.x) {
			if(_isFacingRight) {
				Flip ();
				_isFacingRight = false;
			}
		} else {
			if(!_isFacingRight) {
				Flip ();
				_isFacingRight = true;
			}
		}
	}

	void Flip() {
		transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
	}
	
	void LaunchProjectile() {
		if (!GameState.Instance.playing) {
			return;
		}

		if(Vector2.Distance(transform.position, target.transform.position) <= 15f)
		{
			GuidedMissile shot = Instantiate(prefabShot, transform.position, Quaternion.identity) as GuidedMissile;
			shot.target = target;
		}
	}

	void OnCollisionEnter2D(Collision2D collision)
	{
		if(collision.gameObject.layer == LayerMask.NameToLayer("Death"))
		{
			Destroy(gameObject);
		}
	}

	public void ApplyDamage(int amount)
	{
		health -= amount;
	}
}
