﻿using UnityEngine;
using System.Collections;

public class ExplodingEnemy : MonoBehaviour {

	public int speed = 4;
	
	// Update is called once per frame
	void Update ()
	{
		if (!GameState.Instance.playing) {
			return;
		}

		transform.Translate(Vector3.left * Time.deltaTime * speed, Camera.main.transform);
	}
}
