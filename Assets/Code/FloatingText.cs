﻿using UnityEngine;
using System.Collections;

public class FloatingText : MonoBehaviour
{
	private float _alpha = 1.0f;

	public string text;

	public GUIStyle style;
	public float duration = 1.5f;
	public float scrollSpeed = 0.2f;


	void Awake ()
	{
		style = new GUIStyle();
		style.fontSize = 14;
		style.fontStyle = FontStyle.Bold;
		style.normal.textColor = Color.white;
		style.alignment = TextAnchor.MiddleCenter;
	}
	


	void Update ()
	{
		Color color = style.normal.textColor;

		if (_alpha > 0) {

			transform.position = new Vector2(transform.position.x, transform.position.y + scrollSpeed * Time.deltaTime);

			_alpha -= Time.deltaTime / duration;
			color.a = _alpha;
			style.normal.textColor = color;

		} else {
			Destroy(gameObject);
		}
	}


	void OnGUI()
	{
		Vector2 labelPosition = Camera.main.WorldToScreenPoint(new Vector3(transform.position.x, transform.position.y - 2.0f, transform.position.z));
		GUI.Label (new Rect (labelPosition.x, labelPosition.y, 0, 0), text, style);
	}
}
