﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour
{
//	public WorldController worldController;
	public CharacterController2D playerController;
	public GameObject screenFader;
	public GameObject prefabGameOver;
	public ParallaxBackground parallaxBack;
	public string currentLevel;

	private float _fadeSpeedBlack = 4f;
	private float _fadeSpeedClear = 6f;
	private bool _fadingToBlack = false;
	private bool _fadingToClear = false;


	void Awake()
	{
		screenFader.guiTexture.pixelInset = new Rect(0f, 0f, Screen.width, Screen.height);
	}


	void Start()
	{
		GameState.Instance.currentLevel = currentLevel;
		prefabGameOver.SetActive(false);
		GameState.Instance.StartGame();
	}


	void Update()
	{
		if(GameState.Instance.health <= 0) {
			if(GameState.Instance.playing)
			{
				prefabGameOver.SetActive(true);
			}
			GameState.Instance.EndGame();
		}

		if (_fadingToBlack) {
			FadeToBlack();
		}

		if (_fadingToClear) {
			FadeToClear();
		}
	}


	public bool CanSwitchWorlds()
	{
		return !_fadingToBlack && !_fadingToClear;
	}

	public void SwitchWorlds()
	{
		InvokeRepeating("StartFadeBlack", 0.6f, 0f);
		GameState.Instance.PauseGame();
	}

	void StartFadeBlack()
	{
		_fadingToBlack = true;
	}


	void FadeToClear()
	{
		screenFader.guiTexture.pixelInset = new Rect(0f, 0f, Screen.width, Screen.height);
		screenFader.guiTexture.color = Color.Lerp(screenFader.guiTexture.color, Color.clear, _fadeSpeedClear * Time.deltaTime);
		
		if (screenFader.guiTexture.color.a <= 0.1f) {
			screenFader.guiTexture.color = Color.clear;
			screenFader.guiTexture.enabled = false;
			_fadingToClear = false;
			GameState.Instance.StartGame();
		}
	}
	
	void FadeToBlack()
	{
		screenFader.guiTexture.pixelInset = new Rect(0f, 0f, Screen.width, Screen.height);
		screenFader.guiTexture.enabled = true;
		screenFader.guiTexture.color = Color.Lerp(screenFader.guiTexture.color, Color.black, _fadeSpeedBlack * Time.deltaTime);
		
		if (screenFader.guiTexture.color.a >= 0.8f) {
			_fadingToBlack = false;

			float distance = playerController.Switch();
			parallaxBack.Switch(distance);

			_fadingToClear = true;
		}
	}

}
