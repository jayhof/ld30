﻿using UnityEngine;
using System.Collections;

public class GameRestart : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.anyKey)
		{
			GameState.Instance.StartGame();
			GameState.Instance.ammo = 0;
			GameState.Instance.fuel = 0f;
			GameState.Instance.health = 100f;

			Application.LoadLevel(GameState.Instance.currentLevel);
		}
	}
}
