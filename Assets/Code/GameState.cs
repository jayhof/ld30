﻿using System.Collections;

public sealed class GameState
{
	public enum WorldType
	{
		First,
		Second
	}

	public int score { get; private set; }
	public int ammo = 0;
	public string currentLevel;
	public float fuel = 0;
	public float health = 100.0f;
	public int level { get; private set; }
	public WorldType world { get; private set; }

	public bool playing { get; private set; }

	private static GameState _instance;
	public static GameState Instance
	{
		get
		{
			if (_instance == null) _instance = new GameState();
			return _instance;
		}
	}

	private GameState() { }


	public void SetWorld(WorldType newType)
	{
		world = newType;
	}

	public void StartGame()
	{
		playing = true;
	}

	public void PauseGame()
	{
		playing = false;
	}

	public void EndGame()
	{
		playing = false;
	}


	public void ResetState()
	{
		ammo = 0;
		fuel = 0;
		health = 100f;
	}
}
