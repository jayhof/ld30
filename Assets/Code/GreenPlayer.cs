﻿using UnityEngine;
using System.Collections;

public class GreenPlayer : Player
{
	public ParticleSystem prefabParticles;

	private ParticleSystem _particles;

	void Start()
	{
		type = Player.Type.GreenPlayer;
		horizontalSpeed = 6.0f;
		jumpForce = 750.0f;
		jetForce = 50.0f;
	}


	public override void HandlePlayerDependentInput()
	{
		if(_particles != null) {
			_particles.transform.position = transform.position;
			if(isFacingRight) {
				_particles.transform.position = new Vector2(_particles.transform.position.x - 0.3f, _particles.transform.position.y + -0.2f);
			} else {
				_particles.transform.position = new Vector2(_particles.transform.position.x + 0.3f, _particles.transform.position.y + -0.2f);
			}
			if(!_animator.GetBool("isJetting")) {
				Destroy(_particles.gameObject);
				_particles = null;
			}
		}
			

		if(_animator.GetBool("isJetting")) {
			RenderParticles();
		}

		if(_controller.isGrounded) {
			_animator.SetBool("isJetting", false);
			_animator.SetBool("isJumping", false);
		}

		if (Input.GetKeyDown(KeyCode.Space)) {
			if (_controller.isGrounded) {
				_controller.Jump();
			}
		}

		if (Input.GetKey(KeyCode.Space)) {

			// Jump
			if (_controller.isGrounded) {
				_animator.SetBool("isJumping", true);

			// Jet
			} else if (GameState.Instance.fuel > 0f) {
			
				_controller.Jet();

				_animator.SetBool("isJumping", true);
				_animator.SetBool("isJetting", true);

			} else if (GameState.Instance.fuel <= 0f) {

				_animator.SetBool("isJetting", false);

			}
		}

		if (Input.GetKeyUp(KeyCode.Space)) {
			if (_animator.GetBool("isJetting")) {
				_animator.SetBool("isJetting", false);
			}
		}
	}

	private void RenderParticles()
	{
		if(_particles == null)
		{
			_particles = Instantiate(prefabParticles, transform.position, Quaternion.identity) as ParticleSystem;
		}
	}
}
