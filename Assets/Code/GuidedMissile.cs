﻿using UnityEngine;
using System.Collections;

public class GuidedMissile : MonoBehaviour {

	public CharacterController2D target;

	public GameObject explosionEffect;

	public float speed = 100f;

	private Vector2 _target;

	void Start()
	{
		Vector2 player = new Vector2(target.transform.position.x, target.transform.position.y);
		Vector2 diff = player - new Vector2(transform.position.x, transform.position.y);
		diff.Normalize();

		_target = new Vector2(transform.position.x, transform.position.y) + diff * 20;
	}
	
	void Update ()
	{
		if(!GameState.Instance.playing)
			return;

		transform.position = Vector2.MoveTowards(transform.position, _target, Time.deltaTime * speed);

		if(Vector2.Distance(transform.position, _target) <= 0.01f) {
			Explode();
		}
	}

	void Explode()
	{
		GameObject newParticleSystem = Instantiate(explosionEffect, transform.position, Quaternion.identity) as GameObject;

		Destroy(
			newParticleSystem,
			0.7f
		);
		
		Destroy(gameObject);
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag != "Enemy")
		{
			Explode();
		}

		if (coll.gameObject.tag == "Player")
		{
			Explode();

			target.rigidbody2D.velocity = (target.transform.position - transform.position) * 20f;

			GameState.Instance.health -= 12.5f;
		}
		
	}

}
