﻿using UnityEngine;
using System.Collections;

public class HealthBar : MonoBehaviour {

	private float _fuelPercentWidth = 0.0f;
	private RectTransform _rectTransform;

	// Use this for initialization
	void Start () {
		_rectTransform = GetComponent<RectTransform>();
		
		_fuelPercentWidth = _rectTransform.sizeDelta.x / 100;
	}
	
	// Update is called once per frame
	void Update () {
		_rectTransform.sizeDelta = new Vector2(_fuelPercentWidth * GameState.Instance.health, _rectTransform.sizeDelta.y);
	}
}
