﻿using UnityEngine;
using System.Collections;

public class NextLevel : MonoBehaviour {

	public string nextLevelName;
	
	// Update is called once per frame

	void OnCollisionEnter2D(Collision2D coll)
	{
		if(coll.gameObject.tag == "Player")
		{
			GameState.Instance.ResetState();
			Application.LoadLevel(nextLevelName);
		}
	}
}
