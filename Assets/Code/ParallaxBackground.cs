﻿using UnityEngine;
using System.Collections;

public class ParallaxBackground : MonoBehaviour
{
	private float _backgroundScrollSpeed = 0.1f;
//	private float _middlegroundScrollSpeed = 2.0f;
//	private float _foregroundScrollSpeed = 3.5f;

	private Vector3 _lastTargetPosition;

	public Transform background;
//	public Transform middleground;
//	public Transform foreground;
	public GameObject Up;
	public GameObject Down;

	public Transform target;

	private bool _switched = false;
	// Use this for initialization
	void Start ()
	{
		Up.SetActive(true);
		Down.SetActive(false);
		background.position = new Vector3(target.position.x, target.position.y, background.position.z);
		_lastTargetPosition = target.position;
	}
	
	// Update is called once per frame
	void Update ()
	{
		Vector3 diff = target.position - _lastTargetPosition;
		_lastTargetPosition = target.position;

		Vector2 newBackgroundPos = new Vector2(background.position.x, background.position.y);
		newBackgroundPos.x -= diff.x * _backgroundScrollSpeed;
		newBackgroundPos.y -= diff.y * _backgroundScrollSpeed;

		background.position = new Vector3(newBackgroundPos.x, newBackgroundPos.y, background.position.z);


//		Vector2 newMiddlePos = new Vector2(middleground.position.x, middleground.position.y);
//		newMiddlePos.x -= diff.x * _middlegroundScrollSpeed;
//		
//		middleground.position = new Vector3(newMiddlePos.x, newMiddlePos.y, middleground.position.z);
//
//		Vector2 newForegroundPos = new Vector2(foreground.position.x, foreground.position.y);
//		newForegroundPos.x -= diff.x * _foregroundScrollSpeed;
//		
//		foreground.position = new Vector3(newForegroundPos.x, newForegroundPos.y, foreground.position.z);
	}

	public void Switch(float distance)
	{
		if(_switched)
		{
			_switched = false;
			Up.SetActive(true);
			Down.SetActive(false);
			background.position = new Vector3(background.position.x, background.position.y - distance, background.position.z);
		}
		else
		{
			_switched = true;
			Up.SetActive(false);
			Down.SetActive(true);
			background.position = new Vector3(background.position.x, background.position.y - distance, background.position.z);
		}
	}
}
