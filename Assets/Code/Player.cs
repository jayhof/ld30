﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
	public enum Type {
		RedPlayer,
		GreenPlayer
	}

	public bool isFacingRight { get; private set; }
	private Transform _transform;
	public CharacterController2D _controller { get; private set; }
	public Animator _animator { get; private set; }
	private float _normalizedHorizontalSpeed;

	public Type type;
	public float horizontalSpeed;
	public float jumpForce;
	public float jetForce;
	public float AccelerationOnGround = 5.0f;
	public float AccelerationInAir = 2.5f;

	public Transform groundCheck;
	public Transform wallCheck;


	void Awake()
	{
		isFacingRight = true;

		_controller = GetComponentInParent<CharacterController2D>();
		_transform = GetComponent<Transform>();
		_animator = GetComponentInChildren<Animator>();
	}


	void Update()
	{
		if (!GameState.Instance.playing) {
			return;
		}

		HandleInput();
		HandlePlayerDependentInput();

		var movementFactor = _controller.isGrounded ? AccelerationOnGround : AccelerationInAir;
		_controller.SetHorizontalForce(_normalizedHorizontalSpeed * horizontalSpeed);
	}


	void HandleInput()
	{
		float horizontalInput = Input.GetAxis("Horizontal");
		_normalizedHorizontalSpeed = 0.0f;

		if (Mathf.Abs(horizontalInput) > 0) {

			if(_controller.isGrounded)
			{
				_animator.SetBool("isWalking", true);
			}
			else
			{
				_animator.SetBool("isWalking", false);
			}

			if (horizontalInput > 0) {
				if (!_controller.isWalled) {
					_normalizedHorizontalSpeed = 1.0f;
				}
				if (!isFacingRight) {
					Flip();
				}
			} else {
				if (!_controller.isWalled) {
					_normalizedHorizontalSpeed = -1.0f;
				}
				if (isFacingRight) {
					Flip();
				}
			}

		} else {
			_animator.SetBool("isWalking", false);
		}

		if (_controller.isInRipple && _controller.gameController.CanSwitchWorlds() && Input.GetKeyUp(KeyCode.T))
		{
			_animator.SetTrigger("desolve");
			_controller.gameController.SwitchWorlds();
		}
	}


	public virtual void HandlePlayerDependentInput()
	{
		return;
	}


	private void Flip()
	{
		_transform.localScale = new Vector3(-_transform.localScale.x, _transform.localScale.y, _transform.localScale.z);
		isFacingRight = _transform.localScale.x > 0;
	}
}
