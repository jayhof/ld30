﻿using UnityEngine;
using System.Collections;

public class RedPlayer : Player
{
	public GameObject bullet;
	public Transform gunMuzzle;


	void Start()
	{
		type = Player.Type.RedPlayer;
		horizontalSpeed = 5f;
		jumpForce = 750.0f;
		jetForce = 0f;
	}


	public override void HandlePlayerDependentInput()
	{
		if(_controller.isGrounded)
		{
			_animator.SetBool("isJumping", false);
		}

		if (_controller.isGrounded && Input.GetKeyDown(KeyCode.Space)) {
			_controller.Jump();
			_animator.SetBool("isWalking", false);
		}
		if ( ! _controller.isGrounded && Input.GetKey(KeyCode.Space)) {
			_animator.SetBool("isJumping", true);
		}


		if (Input.GetKeyDown(KeyCode.E)) {
			FireWeapon();
		}
	}

	public void FireWeapon()
	{
		if (GameState.Instance.ammo > 0)
		{
			GameState.Instance.ammo -= 1;

			GameObject newBullet = Instantiate(bullet, new Vector3(gunMuzzle.position.x, gunMuzzle.position.y, gunMuzzle.position.z), Quaternion.identity) as GameObject;
			Rigidbody2D bulletRigid = newBullet.transform.GetComponent<Rigidbody2D>();
			
			if (isFacingRight) {
				bulletRigid.velocity = new Vector2(10.0f, 0f);
			} else {
				newBullet.GetComponent<Bullet>().Flip();
				bulletRigid.velocity = new Vector2(-10.0f, 0f);
			}
		}
	}
}
