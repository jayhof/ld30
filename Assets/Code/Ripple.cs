﻿using UnityEngine;
using System.Collections;

public class Ripple : MonoBehaviour
{
	public GameObject exit;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}


	void OnDrawGizmos()
	{
		if (exit != null) {
			Gizmos.color = Color.red;
			Gizmos.DrawLine(transform.position, exit.transform.position);
		}
	}
}
