﻿using UnityEngine;
using System.Collections;

public class Switch : MonoBehaviour {

	private bool isTriggered = false;

	public GameObject target;

	public GameObject explosionPrefab;

	public Sprite up;

	public Sprite down;

	// Use this for initialization
	void Start () {
		gameObject.GetComponent<SpriteRenderer>().sprite = up;
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if(coll.gameObject.tag == "Player" && !isTriggered)
		{
			isTriggered = true;

			gameObject.GetComponent<SpriteRenderer>().sprite = down;
			
			Destroy(target);
			
			GameObject newParticleSystem = Instantiate(explosionPrefab, target.transform.position, Quaternion.identity) as GameObject;
			
			Destroy(
				newParticleSystem,
				0.7f
			);
		}
	}


	void OnDrawGizmos()
	{
		if (target != null) {
			Gizmos.color = Color.blue;
			Gizmos.DrawLine(transform.position, target.transform.position);
		}
	}
}
