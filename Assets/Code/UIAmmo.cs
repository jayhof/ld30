﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIAmmo : MonoBehaviour
{
	private int _spriteIndex;
	private Image _imageComponent;

	public Sprite[] sprites;


	// Use this for initialization
	void Start ()
	{
		_imageComponent = GetComponent<Image>();
		_spriteIndex = 0;
	}
	
	// Update is called once per frame
	void Update ()
	{
		int newIndex = Mathf.Clamp(GameState.Instance.ammo, 0, sprites.Length - 1);
		if (newIndex != _spriteIndex) {
			_spriteIndex = newIndex;
			_imageComponent.sprite = sprites[_spriteIndex];
		}
	}
}
