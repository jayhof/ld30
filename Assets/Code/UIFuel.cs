﻿using UnityEngine;
using System.Collections;

public class UIFuel : MonoBehaviour
{
	private float _fuelPercentWidth = 0.0f;
	private RectTransform _rectTransform;


	void Start ()
	{
		_rectTransform = GetComponent<RectTransform>();

		_fuelPercentWidth = _rectTransform.sizeDelta.x / 100;
	}
	
	// Update is called once per frame
	void Update ()
	{
		_rectTransform.sizeDelta = new Vector2(_fuelPercentWidth * GameState.Instance.fuel, _rectTransform.sizeDelta.y);
	}
}
