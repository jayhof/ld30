![Quantum Leap](https://bytebucket.org/jayhof/ld30/raw/871dc68a3c07c07fb687ab9eed46cd313d44f900/Assets/Sprites/title.png)

# LD30 - Quantum Leap - Jam Entry

Quantum Leap is a action puzzle platformer created by Johannes Hofmann (@jayhof) and Bastian Hofmann (@HofmannBastian) during the 72 hour Ludum Dare game jam.
Both of us never wrote a game in Unity before and we certainly learned a lot. We even have a super hard boss fight at the end ;)

All assets (except music) are handcrafted by us during the 72 hours.

Good luck to all the other developers!


## Play

[Play Quantum Leap](http://ld30.phpublic.de)

## Story

The world is being taken over by evil forces, you have to unleash your evil side on the enemies and solve puzzels.

*Kick ass and solve puzzles*


### Tools used

- Unity3d
- Sublime Text
- Photoshop
- SFXR
- aseprite
- Tiled (map editor)
- XUni-TMX
- Brain and fingers, sometimes not at the same time




#### Credits

*Music:*
- "At Last" Android128 (Andrey) http://opengameart.org/content/at-last
- "Delusion" Alexandr Zhelanov http://opengameart.org/content/delusion
- "It's not my fault" Alexandr Zhelanov http://opengameart.org/content/its-not-my-fault
